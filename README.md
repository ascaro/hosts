#### 1  
Price: 12€ / Month  
2 Dedicated Cores  
8GB RAM  
50GB SSD  
100MBit/s Connection  
2TB Traffic / Month  
Location: Germany  

___

#### 2  
Price: 15,99€ / Month  
2 Virtual Cores  
4GB RAM  
150GB HDD  
1Gbit/s Connection  
2TB Traffic / Month  
Location: Switzerland  

___

#### 3  
Price: 13,99€ / Month  
4 Virtual Cores  
3GB RAM  
120GB HDD  
100MBit/s Connection  
Unlimited Traffic  
Location: France  

___

#### 4  
Price: 13,49$ / Month  
2 Virtual Cores  
8GB RAM  
40GB SSD  
100MBit/s Connection  
Unlimited Traffic  
Location: France  

___

#### 5  
Price: 22$ / Month  
4 Dedicated Cores  
2GB RAM  
95GB SSD  
1Gbit/s Connection  
4TB Traffic / Month  
Location: Netherlands, Romania, Switzerland, Sweden

___

#### 6  
Price: 13,75€  
2 Virtual Cores  
4GB RAM  
60GB HDD  
10Gbit/s Connection  
Unlimited Traffic  
Location: Switzerland

___

#### 7  
Price: 19,99$ / Month    
2 Dedicated Cores  
4GB RAM  
40GB SSD  
1Gbit/s Connection  
10TB Traffic / Month  
Location: Switzerland
