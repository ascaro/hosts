# Virtual Private Server I would use

[FSIT.com](https://www.fsit.com/server/vps-vserver-kvm)
Product Name: KVM - 4GB
Price: 15,99€ / Month
Cores: 2 vCores
RAM: 4GB
Storage: 150GB HDD
Connection: 1Gbit/s
Traffic: 2000GB / 2TB
IPv4: 1
IPv6: Yes
Location: Swiss
Setup Fee: 0€

___

[PHP-Friends.de](https://php-friends.de/vserver-ssd)
Product Name: vServer M SSD G2
Price: 12€
CPU: Intel Xeon E5-2630 v3
Cores: 2 Dedicated Cores
RAM: 8GB
Storage: 50GB SSD
Connection: 100MBit/s
Traffic: 2TB
IPv4: 1
IPv6: Yes
Location: Germany
Setup Fee: 0€

___
